# prueba-sun-media

Prueba tecnica para sun media por Jose Cordoba

## Vista de equipos

![Screenshot](src/assets/screenshots/equipos.png)

## Vista de Detalle

![Screenshot](src/assets/screenshots/detalleEquipo.png)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import VideoBg from 'vue-videobg'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import App from './App.vue'

Vue.use(BootstrapVue)
Vue.component('video-bg', VideoBg)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
